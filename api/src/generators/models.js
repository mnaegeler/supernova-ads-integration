const fs = require('fs').promises
const path = require('path')
const prettier = require("prettier")

// Paths
const modelsPath = path.resolve(__dirname, '..', 'app', 'models')
const metadataPath = path.resolve(__dirname, '..', 'app', 'metadata')
const modelTemplatePath = path.resolve(__dirname, '..', 'templates', 'model.js.tpl')

const modelsNotToUpdate = [
  // !important! User should not be updated because of hooks and custom functions...
  'User', 
  // There is a case with ChildViewId that should be resolved to work properly
  'ViewField',
  // There is a case with defaultValue that should be resolved to work properly
  'Language',
]

async function readAndParseFile(path) {
  const file = await fs.readFile(path, 'utf-8')
  return JSON.parse(file)
}

function order(a, b) {
  if (a < b) {
    return -1
  }

  if (a > b) {
    return 1
  }

  return 0
}

async function init () {
  const metaFiles = await fs.readdir(metadataPath)
  const modelsToUpdate = metaFiles.filter(modelName => modelsNotToUpdate.includes(modelName) === false)

  const modelTemplateFile = await fs.readFile(modelTemplatePath, 'utf-8')

  await Promise.all(
    modelsToUpdate.map(async (metaFile) => {
      const modelData = await readAndParseFile(path.resolve(metadataPath, metaFile, 'Model.json'))

      const fieldFiles = await fs.readdir(path.resolve(metadataPath, metaFile, 'ModelFields'))

      const fields = []
      const relations = []

      await Promise.all(
        fieldFiles.map(async (fieldFile) => {
          const field = await readAndParseFile(path.resolve(metadataPath, metaFile, 'ModelFields', fieldFile))
          if (!['createdAt', 'updatedAt', 'deletedAt', 'OwnerId'].includes(field.name)) {
            if (['belongsTo', 'hasMany'].includes(field.type)) {
              relations.push(`{#modelName}.${field.type}(models.${field.targetModel});`)
            } else {
              fields.push(`${field.name}: ${field.modelFieldType},`)
            }
          }
        })
      )

      let parsed = modelTemplateFile
      parsed = parsed.replace(/\{#fields\}/g, fields.sort(order).join(''))
      parsed = parsed.replace(/\{#relations\}/g, relations.sort(order).join(''))
      parsed = parsed.replace(/\{#modelName\}/g, modelData.modelName)

      const pretty = prettier.format(parsed, { parser: 'babel' })
      await fs.writeFile(path.resolve(modelsPath, `${modelData.modelName}.js`), pretty)
  })
  )
}

init()
