const fs = require('fs').promises
// const util = require('util')
const path = require('path')
const { Model, ModelField, View, ViewField, ViewAction, ViewRowAction, ModelValidation } = require('../app/models')

const metadataPath = path.resolve(__dirname, '..', 'app', 'metadata')

// If you want to filter out models
const modelsNotToUpdate = [
]

async function readAndParseFile(path) {
  const file = await fs.readFile(path, 'utf-8')
  return JSON.parse(file)
}

async function init () {
  const metaModels = await fs.readdir(metadataPath)
  const modelsToUpdate = metaModels.filter(modelName => modelsNotToUpdate.includes(modelName) === false)

  // This import step needs to be done in two steps because of Foreign Keys on ChildViewId
  
  // Models, Models Fields and Views
  try {
    await Promise.all(
      modelsToUpdate.map(async (modelName) => {
        const model = await readAndParseFile(path.resolve(metadataPath, modelName, 'Model.json'))
        
        // Model
        await Model.upsert(model)

        let metaModelFields = []
        let metaModelValidations = []
        let metaViews = []
        
        try {
          metaModelFields = await fs.readdir(path.resolve(metadataPath, modelName, 'ModelFields'))
        } catch (e) {}

        try {
          metaModelValidations = await fs.readdir(path.resolve(metadataPath, modelName, 'ModelValidations'))
        } catch (e) {}

        try {
          metaViews = await fs.readdir(path.resolve(metadataPath, modelName, 'Views'))
        } catch (e) {}

        await Promise.all([
          ...metaModelFields.map(async (file) => {
            const data = await readAndParseFile(path.resolve(metadataPath, modelName, 'ModelFields', file))
            await ModelField.upsert(data)
          }),

          ...metaModelValidations.map(async (file) => {
            const data = await readAndParseFile(path.resolve(metadataPath, modelName, 'ModelValidations', file))
            await ModelValidation.upsert(data)
          }),

          ...metaViews.map(async (file) => {
            const data = await readAndParseFile(path.resolve(metadataPath, modelName, 'Views', file))
            await View.upsert(data)
          }),
        ])
      })
    )
  } catch (e) {
    console.error(e)
    throw new Error(e)
  }

  // View Fields, Actions and Row Actions
  try {
    await Promise.all(
      modelsToUpdate.map(async (modelName) => {
        let metaViewFields = []
        let metaViewActions = []
        let metaViewRowActions = []
        
        try {
          metaViewFields = await fs.readdir(path.resolve(metadataPath, modelName, 'ViewFields'))
        } catch (e) {}

        try {
          metaViewActions = await fs.readdir(path.resolve(metadataPath, modelName, 'ViewActions'))
        } catch (e) {}

        try {
          metaViewRowActions = await fs.readdir(path.resolve(metadataPath, modelName, 'ViewRowActions'))
        } catch (e) {}

        await Promise.all([
          ...metaViewFields.map(async (file) => {
            const data = await readAndParseFile(path.resolve(metadataPath, modelName, 'ViewFields', file))
            await ViewField.upsert(data)
          }),

          ...metaViewActions.map(async (file) => {
            const data = await readAndParseFile(path.resolve(metadataPath, modelName, 'ViewActions', file))
            await ViewAction.upsert(data)
          }),

          ...metaViewRowActions.map(async (file) => {
            const data = await readAndParseFile(path.resolve(metadataPath, modelName, 'ViewRowActions', file))
            await ViewRowAction.upsert(data)
          }),
        ])
      })
    )
  } catch (e) {
    console.error(e)
    throw new Error(e)
  }
}

process.argv.forEach(val =>  {
  if (val === '-s') {
    init()
  }
})

module.exports = {
  init
}
