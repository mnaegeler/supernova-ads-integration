const { Op } = require('sequelize')

const { Model, ModelPermission } = require('../models')

module.exports = {
  async hasPermission (modelName, action, user) {
    return await Model.findOne({
      where: {
        modelName
      },

      include: {
        model: ModelPermission,
        required: !user.Role.hasFullAccess,
        where: {
          action,
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })
  }
}
