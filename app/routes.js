import { wrap } from 'svelte-spa-router'

import Login from './views/default/Login.svelte'
import ResetPassword from './views/default/ResetPassword.svelte'
import SetPassword from './views/default/SetPassword.svelte'
import NotFound from './views/default/NotFound.svelte'
import Layout from './views/default/Layout.svelte'

function userIsLoggedIn () {
  const user = localStorage.getItem('user')
  const token = localStorage.getItem('authToken')

  return !!user && !!token
}

function verifyLogin (detail) {
  detail.userIsLoggedIn = userIsLoggedIn()
  detail.routeIsPrivate = true
  return detail.userIsLoggedIn
}

const routes = {
  '/login': Login,
  '/reset-password': ResetPassword,
  '/set-password/:token': SetPassword,
  '/': wrap(Layout, verifyLogin),
  '/:view': wrap(Layout, verifyLogin),
  '/:view/:id': wrap(Layout, verifyLogin),
  '/*': NotFound,
}

export { routes }
